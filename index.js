import Papa from "papaparse";

import csvPath from "./data/users.csv";
import store from "./src/store";
import renderView from "./src/views";
import updateBarChart from "./src/barchart";
import updatePieChart from "./src/piechart";
import pureActions from "./src/actions";

function applyAction(action, params) {
  const newState = action(store, params);

  // Here, we mutate to keep our reference to the original store object
  // valid:
  for (let k in newState) {
    store[k] = newState[k];
  }

  render();
}

const actions = {};
for (let k in pureActions) {
  const pureAction = pureActions[k];
  actions[k] = function(params) {
    applyAction(pureAction, params);
  };
}

Papa.parse(csvPath, {
  header: true,
  download: true,
  skipEmptyLines: true,
  complete: function(results) {
    store.data = results.data;
    store.meta.fields = results.meta.fields;

    init();
  }
});

render(); // Initial rendering

function init() {
  actions.setQueryField({selectedField: store.meta.fields[0]}); // When the data is loaded
}

function render() {
  const root = document.getElementById("form-root");
  root.innerHTML = "";

  const view = renderView(store, actions);
  root.appendChild(view);

  // Deal with charts:
  updatePieChart(store);
  updateBarChart(store);
}

window.app = {
  store,
  actions
};
