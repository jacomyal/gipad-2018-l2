To make the project work:
  - Install Node.js and NPM
  - Clone the repository
  - Go into the repository from a command line
  - Type `npm install` to install dependencies
  - Then type `npm run build` to run it

Also, if you pull the project, **don't forget to re-run `npm install`**, in case there are new dependencies to fetch.
