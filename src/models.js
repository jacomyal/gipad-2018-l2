function topN(list, field, n) {

  // 1. Count values:
  const dict = {};
  list.forEach(function(user) {
    const value = user[field];

    if (dict[value]) {
      dict[value]++;
    } else {
      dict[value] = 1;
    }
  });

  // 2. Transform values into an ordonned data structure:
  const valuesArray = [];
  for (let k in dict) {
    valuesArray.push({ value: k, count: dict[k] });
  }

  // 3. Sort values:
  const sortedArray = valuesArray.sort(function(a, b) {
    return b.count - a.count;
  });

  // 4. Pick only N first values:
  return sortedArray.slice(0, n);
}

function top5(list, field) {
  return topN(list, field, 5)
}

function top10(list, field) {
  return topN(list, field, 10)
}

export default {
  topN,
  top5,
  top10
};
