import models from "./models";

function renderForm(state, actions) {
  const root = document.createElement("div");

  // Block title:
  const h3 = document.createElement("h3");
  h3.innerHTML = "Query:";
  root.appendChild(h3);

  // Fields selection:
  const fieldsSelect = document.createElement("select");
  const selectedValue = state.query.selectedField;
  state.meta.fields.forEach(function(field) {
    const option = document.createElement("option");
    option.innerHTML = field;
    option.value = field;
    if (field === selectedValue) option.selected = true;
    fieldsSelect.appendChild(option);
  });
  fieldsSelect.addEventListener("change", function(e) {
    const newValue = e.target.value;
    actions.setQueryField({selectedField: newValue});
  });
  root.appendChild(fieldsSelect);

  // Display mode selection:
  const modeSelect = document.createElement("select");
  const selectedMode = state.query.display;
  ["top5", "top10"].forEach(function(mode) {
    const option = document.createElement("option");
    option.innerHTML = mode;
    option.value = mode;
    if (mode === selectedMode) option.selected = true;
    modeSelect.appendChild(option);
  });
  modeSelect.addEventListener("change", function(e) {
    const newValue = e.target.value;
    actions.setDisplay({display: newValue});
  });
  root.appendChild(modeSelect);

  return root;
}

export default function render(state, actions) {
  const root = document.createElement("div");

  root.appendChild(renderForm(state, actions));

  return root;
}
