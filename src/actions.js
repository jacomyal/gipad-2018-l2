function setQueryField(state, params) {
  // Here, we use the ES7 spread operator, to return a new state
  // without mutating the old one:
  return {
    ...state,
    query: {
      ...state.query,
      selectedField: params.selectedField
    }
  };
}

function setDisplay(state, params) {
  return {
    ...state,
    query: {
      ...state.query,
      display: params.display
    }
  };
}

export default {
  setQueryField,
  setDisplay
};
