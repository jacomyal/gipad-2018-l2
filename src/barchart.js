import * as d3 from "d3";
import { scaleLinear } from "d3-scale";

import models from "./models";

// Visual settings:
const CHART_WIDTH = 600;
const CHART_HEIGHT = 400;
const MARGIN = 1;

const chart = d3.select("#barchart-root")
  .attr("width", CHART_WIDTH)
  .attr("height", CHART_HEIGHT);
const y = scaleLinear()
  .range([0, CHART_HEIGHT]);

function updateChart(state) {
  // Find the exact list of elements we want to display, from the related model:
  const mode = state.query.display;
  const model = models[mode];
  const list = model(state.data, state.query.selectedField);

  // Update the domain, to keep fitting the whole chart container width:
  y.domain([0, d3.max(list, element => element.count)]);

  const barWidth = CHART_WIDTH / list.length;

  const bar = chart.selectAll("g")
    .data(list);

  // ADD MISSING GROUPS:
  // -------------------
  // The bar.enter() selection selects the data that has just been added,
  // compared to the previous dataset. It allows us to deal properly on
  // how bars should be created. Here, we choose that a new bar is always
  // with a width of 0, and that it will grow after:
  const enteringBars = bar
    .enter()
      .append("g")
      .style("opacity", 0);
  enteringBars
    .append("rect");
  enteringBars
    .append("text")
      .attr("text-anchor", "middle")
      .attr("x", 0)
      .attr("y", 10);

  // DELETE EXCEDING GROUPS:
  // -----------------------
  // The bar.exit() selection selects the data that has just been removed,
  // compared to the previous dataset. It allows us to deal properly on
  // how bars should be deleted. Here, we choose to animate the width of
  // disappearing bars to 0:
  const exitingBars = bar
    .exit()
      .transition()
      .duration(500)
      .remove()
      .style("opacity", 0);

  // UPDATE ALL EXISTING GROUPS:
  // ---------------------------
  // The bar.merge(newGroup) selection selects all existing bars AND the one we
  // just created. It allows us to update and animate the bars width to fit the
  // new values:
  const updatingBars = bar
    .merge(enteringBars)
    .transition()
      .duration(500)
      .style("opacity", 1)
      .attr(
        "transform",
        (element, i) => `translate(${ i * barWidth }, ${CHART_HEIGHT - y(element.count)})`
      );
  updatingBars
    .select("rect")
    .attr("height", element => y(element.count))
    .attr("width", barWidth - 1);
  updatingBars
    .select("text")
    .text(element => element.value)
    .attr("dx", barWidth / 2);
}

export default updateChart;
