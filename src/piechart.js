import * as d3 from "d3";
import { scaleLinear } from "d3-scale";

import models from "./models";

// Visual settings:
const CHART_SIZE = 600;
const RADIUS = 280;
const COLORS = [
  "#cc4b3b",
  "#45b2c4",
  "#d24796",
  "#57a95b",
  "#a158c6",
  "#999a3e",
  "#6575c8",
  "#c88441",
  "#c182c2",
  "#c05f71"
];

const arcs = d3.pie()
  .value(element => element.count);
const arc = d3.arc()
  .innerRadius(RADIUS / 2)
  .outerRadius(RADIUS);

const chart = d3.select("#piechart-root")
  .attr("width", CHART_SIZE)
  .attr("height", CHART_SIZE)
  .append("g")
    .attr("transform", `translate(${CHART_SIZE / 2}, ${CHART_SIZE / 2})`);

function updateChart(state) {
  // Find the exact list of elements we want to display, from the related model:
  const mode = state.query.display;
  const model = models[mode];
  const list = model(state.data, state.query.selectedField);

  const arcsData = arcs(list);

  const path = chart.selectAll("g")
    .data(arcsData);

  // ADD MISSING GROUPS:
  // -------------------
  // The path.enter() selection selects the data that has just been added,
  // compared to the previous dataset. It allows us to deal properly on
  // how bars should be created. Here, we choose that a new bar is always
  // with a width of 0, and that it will grow after:
  const enteringPath = path
    .enter()
      .append("g");
  enteringPath
    .append("path")
      .style("opacity", 0)
      .attr("fill", (element, i) => COLORS[i % COLORS.length]);

  // DELETE EXCEDING GROUPS:
  // -----------------------
  // The path.exit() selection selects the data that has just been removed,
  // compared to the previous dataset. It allows us to deal properly on
  // how bars should be deleted. Here, we choose to animate the width of
  // disappearing bars to 0:
  const exitingPath = path
    .exit()
      .transition()
      .duration(500)
      .remove();
  exitingPath
    .select("path")
    .style("opacity", 0);

  // UPDATE ALL EXISTING GROUPS:
  // ---------------------------
  // The path.merge(newGroup) selection selects all existing bars AND the one we
  // just created. It allows us to update and animate the bars width to fit the
  // new values:
  const updatingPath = path
    .merge(enteringPath)
    .transition()
      .duration(500);
  updatingPath
    .select("path")
    .attr("d", arc)
    .style("opacity", 1);
}

export default updateChart;
